package by.epam.jewelry.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.log4j.Logger;

import by.epam.jewelry.entity.AbstractGem;
import by.epam.jewelry.entity.PreciousStone;
import by.epam.jewelry.entity.SemiPreciousStone;
import by.epam.jewelry.runner.Runner;

public class NecklaceStAXBuilder extends AbstractNecklaceBuilder {

	private final static Logger LOG = Logger.getLogger(Runner.class);
	private List<AbstractGem> necklace = new ArrayList<AbstractGem>();
	private XMLInputFactory inputFactory;

	public NecklaceStAXBuilder() {
		inputFactory = XMLInputFactory.newInstance();
	}
	
	public NecklaceStAXBuilder(List<AbstractGem> necklace) {
		super(necklace);
	}

	public List<AbstractGem> getNecklace() {
		return necklace;
	}

	public void buildNecklace(String fileName) {
		FileInputStream inputStream = null;
		XMLStreamReader reader = null;
		String name;
		try {
			inputStream = new FileInputStream(new File(fileName));
			reader = inputFactory.createXMLStreamReader(inputStream);
			while (reader.hasNext()) {
				int type = reader.next();
				if (type == XMLStreamConstants.START_ELEMENT) {
					name = reader.getLocalName();
					if (name.equals(NecklaceEnum.PRECIOUS_STONE.getValue())) {
						PreciousStone gem = buildPreciousStone(reader);
						necklace.add(gem);
					}
					if (name.equals(NecklaceEnum.SEMI_PRECIOUS_STONE.getValue())) {
						SemiPreciousStone gem = buildSemiPreciousStone(reader);
						necklace.add(gem);
					}
				}
			}
		} catch (XMLStreamException ex) {
			LOG.error("StAX parsing error: " + ex.getMessage());
		} catch (FileNotFoundException ex) {
			LOG.error("File " + fileName + " not found!");
		} finally {
			try {
				if (inputStream != null) {
					inputStream.close();
				}
			} catch (IOException e) {
				LOG.error("Impossible to close file " + fileName);
			}
		}
	}

	private PreciousStone buildPreciousStone(XMLStreamReader reader)
			throws XMLStreamException {
		PreciousStone st = new PreciousStone();
		st.setId(reader.getAttributeValue(null, NecklaceEnum.ID.getValue()));
		st.setType(reader.getAttributeValue(null, NecklaceEnum.TYPE.getValue()));
		String name;
		while (reader.hasNext()) {
			int type = reader.next();
			switch (type) {
			case XMLStreamConstants.START_ELEMENT:
				name = reader.getLocalName();
				NecklaceEnum temp = null;
				NecklaceEnum[] values = NecklaceEnum.values();
				for (NecklaceEnum v : values) {
					if (name.equals(v.getValue())) {
						temp = v;
					}
				}
				switch (temp) {
				case TRANSLUCENCY:
					name = getXMLText(reader);
					st.setTranslucency(Integer.parseInt(name));
					break;

				case COST:
					name = getXMLText(reader);
					st.setCost(Double.parseDouble(name));
					break;

				case GRAM_WEIGHT:
					name = getXMLText(reader);
					st.setGramWeight(Double.parseDouble(name));
					break;

				case CARAT_WEIGHT:
					name = getXMLText(reader);
					st.setCaratWeight(Double.parseDouble(name));
					break;

				case PRECIOUS_STONE:
					st.setProperties(getXMLProperties(reader));
					break;

				case COLOR:
					st.setColor();

				default:
					break;
				}
				break;
			case XMLStreamConstants.END_ELEMENT:
				name = reader.getLocalName();
				if (name.equals(NecklaceEnum.PRECIOUS_STONE.getValue())) {
					return st;
				}
				break;
			}
		}
		throw new XMLStreamException("Unknown element in tag Precious Stone");
	}

	private SemiPreciousStone buildSemiPreciousStone(XMLStreamReader reader)
			throws XMLStreamException {
		SemiPreciousStone st = new SemiPreciousStone();
		st.setId(reader.getAttributeValue(null, NecklaceEnum.ID.getValue()));
		st.setType(reader.getAttributeValue(null, NecklaceEnum.TYPE.getValue()));
		String name;
		while (reader.hasNext()) {
			int type = reader.next();
			switch (type) {
			case XMLStreamConstants.START_ELEMENT:
				name = reader.getLocalName();
				NecklaceEnum temp = null;
				NecklaceEnum[] values = NecklaceEnum.values();
				for (NecklaceEnum v : values) {
					if (name.equals(v.getValue())) {
						temp = v;
					}
				}
				switch (temp) {
				case TRANSLUCENCY:
					name = getXMLText(reader);
					st.setTranslucency(Integer.parseInt(name));
					break;

				case COST:
					name = getXMLText(reader);
					st.setCost(Double.parseDouble(name));
					break;

				case GRAM_WEIGHT:
					name = getXMLText(reader);
					st.setGramWeight(Double.parseDouble(name));
					break;

				case CARAT_WEIGHT:
					name = getXMLText(reader);
					st.setCaratWeight(Double.parseDouble(name));
					break;

				case PRECIOUS_STONE:
					st.setProperties(getXMLProperties(reader));
					break;

				case COLOR:
					st.setColor(getXMLText(reader));
					break;
					
				case ORIGIN:
					st.setOrigin();

				default:
					break;
				}
				break;
			case XMLStreamConstants.END_ELEMENT:
				name = reader.getLocalName();
				if (name.equals(NecklaceEnum.SEMI_PRECIOUS_STONE.getValue())) {
					return st;
				}
				break;
			}
		}
		throw new XMLStreamException("Unknown element in tag Semi Precious Stone");
	}

	private AbstractGem.PhysicalProperties getXMLProperties(
			XMLStreamReader reader) throws XMLStreamException {
		AbstractGem.PhysicalProperties properties = new AbstractGem.PhysicalProperties();
		int type;
		String name;
		while (reader.hasNext()) {
			type = reader.next();
			switch (type) {
			case XMLStreamConstants.START_ELEMENT:
				name = reader.getLocalName();
				NecklaceEnum temp = null;
				NecklaceEnum[] values = NecklaceEnum.values();
				for (NecklaceEnum v : values) {
					if (name.equals(v.getValue())) {
						temp = v;
					}
				}
				switch (temp) {

				case HARDNESS:
					name = getXMLText(reader);
					properties.setHardness(Double.parseDouble(name));
					break;

				case SPECIFIC_GRAVITY:
					name = getXMLText(reader);
					properties.setSpecificGravity(Double.parseDouble(name));
					break;

				case CLEAVAGE:
					properties.setCleavage(getXMLText(reader));
					break;

				default:
					break;
				}
				break;
			case XMLStreamConstants.END_ELEMENT:
				name = reader.getLocalName();
				if (name.equals(NecklaceEnum.SEMI_PRECIOUS_STONE.getValue())) {
					return properties;
				}
				break;
			}
		}
		throw new XMLStreamException("Unknown element in tag Properties");
	}

	private String getXMLText(XMLStreamReader reader) throws XMLStreamException {
		String text = null;
		if (reader.hasNext()) {
			reader.next();
			text = reader.getText();
		}
		return text;
	}
}
