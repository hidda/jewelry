package by.epam.jewelry.parser;

public class NecklaceBuilderFactory {

	private enum TypeParser {
		SAX, STAX, DOM
	}

	public AbstractNecklaceBuilder createNecklaceBuilder(String typeParser) {
		TypeParser type = TypeParser.valueOf(typeParser.toUpperCase());
		switch (type) {
		case DOM:
			return new NecklaceDOMBuilder();
		case STAX:
			return new NecklaceStAXBuilder();
		case SAX:
			return new NecklaceSAXBuilder();
		default:
			throw new EnumConstantNotPresentException(type.getDeclaringClass(),
					type.name());
		}
	}

}
