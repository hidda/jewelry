package by.epam.jewelry.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Necklace {

	private List<AbstractGem> gems;

	public Necklace() {
		gems = new ArrayList<AbstractGem>();
	}
	
	public Necklace(List<AbstractGem> gems) {
		this.gems = gems;
	}

	public List<AbstractGem> getNecklace() {
		return Collections.unmodifiableList(gems);
	}

	public void addElement(AbstractGem gem) {
		gems.add(gem);
	}

	public List<AbstractGem> copyNecklace() {
		List<AbstractGem> copiedNecklace = new ArrayList<AbstractGem>();
		for (int i = 0; i < gems.size(); i++) {
			copiedNecklace.add(gems.get(i));
		}
		return copiedNecklace;

	}

}
