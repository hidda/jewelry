package by.epam.jewelry.entity;

public enum Color {

	BLUE, GREEN, YELLOW, COLORLESS, RED, PINK, VIOLET

}
