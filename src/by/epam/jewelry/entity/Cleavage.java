package by.epam.jewelry.entity;

public enum Cleavage {

	NONE, INDISTINCT, GOOD, PERFECT

}
