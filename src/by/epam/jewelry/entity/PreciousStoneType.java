package by.epam.jewelry.entity;

public enum PreciousStoneType {

	DIAMOND, RUBY, EMERALD, SAPPHIRE

}
