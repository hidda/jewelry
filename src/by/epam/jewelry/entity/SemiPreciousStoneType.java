package by.epam.jewelry.entity;

public enum SemiPreciousStoneType {

	BERYL, PERL, AQAMARINE, TOPAZ, OPAL, AMBER, CORAL

}
