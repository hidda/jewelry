package by.epam.jewelry.entity;

public class PreciousStone extends AbstractGem {

	public PreciousStone(PreciousStoneType type, Color color, String id,
			int translucency, double cost, double gramWeight,
			double caratWeight, PhysicalProperties properties) {
		super(id, translucency, cost, gramWeight, caratWeight, properties);
		this.type = type;
		this.color = color;
	}

	public PreciousStone() {
	}

	private PreciousStoneType type;
	private Color color;

	public void setColor() {
		switch (getType()) {
		case DIAMOND:
			this.color = Color.COLORLESS;
			break;

		case EMERALD:
			this.color = Color.GREEN;
			break;

		case RUBY:
			this.color = Color.RED;
			break;

		case SAPPHIRE:
			this.color = Color.BLUE;
			break;
		}
	}

	public Color getColor() {
		return color;
	}

	public PreciousStoneType getType() {
		return type;
	}

	public void setType(String type) {
		type = type.toUpperCase();
		this.type = PreciousStoneType.valueOf(type);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Stone Type: " + getType().name() + "; ");
		sb.append("Stone Color: " + getColor().name() + "; ");
		sb.append("Stone Translucency: " + getTranslucency() + "%; ");
		sb.append("Stone Cost: " + getCost() + " USD; ");
		sb.append("Stone Gram Weight: " + getGramWeight() + "; ");
		sb.append("Stone Carat Weight: " + getCaratWeight());
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PreciousStone other = (PreciousStone) obj;
		if (getGramWeight() != other.getGramWeight())
			return false;
		if (getTranslucency() != other.getTranslucency())
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		return (int) (31 * getGramWeight() + getTranslucency() + ((type == null) ? 0
				: getGramWeight() + getTranslucency() + type.hashCode()));
	}

}
