package by.epam.jewelry.entity;

public class SemiPreciousStone extends AbstractGem {

	public SemiPreciousStone(String type, Color color, String id,
			int translucency, double cost, double gramWeight,
			double caratWeight, PhysicalProperties properties) {
		super(id, translucency, cost, gramWeight, caratWeight, properties);
		this.setType(type);
		this.setOrigin();
		this.color = color;
	}

	public SemiPreciousStone() {
	}

	private SemiPreciousStoneOrigin origin;
	private SemiPreciousStoneType type;
	private Color color;

	public Color getColor() {
		return color;
	}

	public void setColor(String color) {
		color = color.toUpperCase();
		this.color = Color.valueOf(color);
	}

	public SemiPreciousStoneType getType() {
		return type;
	}

	public void setType(String type) {
		type = type.toUpperCase();
		this.type = SemiPreciousStoneType.valueOf(type);
	}

	public SemiPreciousStoneOrigin getOrigin() {
		return origin;
	}

	public void setOrigin() {
		switch (getType()) {
		case AMBER:
		case CORAL:
		case PERL:
			this.origin = SemiPreciousStoneOrigin.ORGANIC;
			break;
		default:
			this.origin = SemiPreciousStoneOrigin.MINERAL;
			break;
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Stone Type: " + getType().name() + "; ");
		sb.append("Stone Color: " + getColor().name() + "; ");
		sb.append("Stone Translucency: " + getTranslucency() + "%; ");
		sb.append("Stone Cost: " + getCost() + " USD; ");
		sb.append("Stone Gram Weight: " + getGramWeight() + "; ");
		sb.append("Stone Carat Weight: " + getCaratWeight());
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SemiPreciousStone other = (SemiPreciousStone) obj;
		if (getGramWeight() != other.getGramWeight())
			return false;
		if (getTranslucency() != other.getTranslucency())
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		return (int) (31 * getGramWeight() + getTranslucency() + ((type == null) ? 0
				: type.hashCode()));
	}

}
