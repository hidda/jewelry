package by.epam.jewelry.parser;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import by.epam.jewelry.entity.AbstractGem;
import by.epam.jewelry.entity.PreciousStone;
import by.epam.jewelry.entity.SemiPreciousStone;

public class NecklaceHandler extends DefaultHandler {

	private final static Logger LOG = Logger.getLogger(NecklaceHandler.class);
	private List<AbstractGem> necklace;
	private PreciousStone currentPreciousGem;
	private SemiPreciousStone currSemiPrecious;
	private NecklaceEnum currentEnum;
	private EnumSet<NecklaceEnum> withText;
	private boolean semiPrecious = false;

	public NecklaceHandler() {
		necklace = new ArrayList<AbstractGem>();
		withText = EnumSet
				.range(NecklaceEnum.TRANSLUCENCY, NecklaceEnum.ORIGIN);
	}

	public List<AbstractGem> getNecklace() {
		return necklace;
	}

	public void startElement(String uri, String localName, String qName,
			Attributes attrs) {
		if ("precious-stone".equals(localName)) {
			currentPreciousGem = new PreciousStone();
			currentPreciousGem.setType(attrs.getValue(1));
			currentPreciousGem.setId(attrs.getValue(0));
		} else if ("semi-precious-stone".equals(localName)) {
			currSemiPrecious = new SemiPreciousStone();
			currSemiPrecious.setType(attrs.getValue(1));
			currSemiPrecious.setId(attrs.getValue(0));
			semiPrecious = true;
		} else {
			NecklaceEnum temp = null;
			NecklaceEnum[] values = NecklaceEnum.values();
			for (NecklaceEnum v : values) {
				if (localName.equals(v.getValue())) {
					temp = v;
				}
			}
			if (withText.contains(temp)) {
				currentEnum = temp;
			}
		}
	}

	public void endElement(String uri, String localName, String qName) {
		if ("precious-stone".equals(localName)) {
			necklace.add(currentPreciousGem);
		}
		if ("semi-precious-stone".equals(localName)) {
			necklace.add(currSemiPrecious);
			semiPrecious = false;
		}
	}

	public void characters(char[] ch, int start, int length) {
		String s = new String(ch, start, length).trim();
		if (currentEnum != null) {
			if (!semiPrecious) {
				switch (currentEnum) {
				case TRANSLUCENCY:
					currentPreciousGem.setTranslucency(Integer.parseInt(s));
					break;

				case COST:
					currentPreciousGem.setCost(Double.parseDouble(s));
					break;

				case GRAM_WEIGHT:
					currentPreciousGem.setGramWeight(Double.parseDouble(s));
					break;

				case CARAT_WEIGHT:
					currentPreciousGem
							.setCaratWeight(Double.parseDouble(s));
					break;

				case HARDNESS:
					currentPreciousGem.getProperties().setHardness(
							Double.parseDouble(s));
					break;

				case SPECIFIC_GRAVITY:
					currentPreciousGem.getProperties().setSpecificGravity(
							Double.parseDouble(s));
					break;

				case CLEAVAGE:
					currentPreciousGem.getProperties().setCleavage(s);
					break;

				case COLOR:
					currentPreciousGem.setColor();
					break;

				default:
					LOG.warn("There is no NecklaceEnum element with this name");
				}
			} else {
				switch (currentEnum) {
				case TRANSLUCENCY:
					currSemiPrecious.setTranslucency(Integer.parseInt(s));
					break;

				case COST:
					currSemiPrecious.setCost(Double.parseDouble(s));
					break;

				case GRAM_WEIGHT:
					currSemiPrecious.setGramWeight(Double.parseDouble(s));
					break;

				case CARAT_WEIGHT:
					currSemiPrecious.setCaratWeight(Double.parseDouble(s));
					break;

				case HARDNESS:
					currSemiPrecious.getProperties().setHardness(
							Double.parseDouble(s));
					break;

				case SPECIFIC_GRAVITY:
					currSemiPrecious.getProperties().setSpecificGravity(
							Double.parseDouble(s));
					break;

				case CLEAVAGE:
					currSemiPrecious.getProperties().setCleavage(s);
					break;

				case COLOR:
					currSemiPrecious.setColor(s);
					break;

				case ORIGIN:
					currSemiPrecious.setOrigin();
					break;

				default:
					LOG.warn("There is no NecklaceEnum element with this name");
				}

			}
			currentEnum = null;
		}
	}
}