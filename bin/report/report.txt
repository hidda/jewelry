Necklace report
Necklace consists of: 

Stone Type: BERYL; Stone Color: GREEN; Stone Translucency: 15%; Stone Cost: 8.0 USD; Stone Gram Weight: 3.0; Stone Carat Weight: 15.0
Stone Type: EMERALD; Stone Color: GREEN; Stone Translucency: 80%; Stone Cost: 25.0 USD; Stone Gram Weight: 2.2; Stone Carat Weight: 11.0
Stone Type: DIAMOND; Stone Color: COLORLESS; Stone Translucency: 95%; Stone Cost: 30.0 USD; Stone Gram Weight: 2.0; Stone Carat Weight: 10.0
Stone Type: BERYL; Stone Color: GREEN; Stone Translucency: 20%; Stone Cost: 9.0 USD; Stone Gram Weight: 3.2; Stone Carat Weight: 16.0
Stone Type: EMERALD; Stone Color: GREEN; Stone Translucency: 85%; Stone Cost: 28.0 USD; Stone Gram Weight: 2.0; Stone Carat Weight: 10.0

Total Cost of the Necklace: 100.0

Total Weight of the Necklace: 62.0

Gems, which translucency is between 20 and 80:

Stone Type: EMERALD; Stone Color: GREEN; Stone Translucency: 80%; Stone Cost: 25.0 USD; Stone Gram Weight: 2.2; Stone Carat Weight: 11.0
Stone Type: BERYL; Stone Color: GREEN; Stone Translucency: 20%; Stone Cost: 9.0 USD; Stone Gram Weight: 3.2; Stone Carat Weight: 16.0

After the sorting the necklace consists of: 

Stone Type: BERYL; Stone Color: GREEN; Stone Translucency: 15%; Stone Cost: 8.0 USD; Stone Gram Weight: 3.0; Stone Carat Weight: 15.0
Stone Type: BERYL; Stone Color: GREEN; Stone Translucency: 20%; Stone Cost: 9.0 USD; Stone Gram Weight: 3.2; Stone Carat Weight: 16.0
Stone Type: EMERALD; Stone Color: GREEN; Stone Translucency: 80%; Stone Cost: 25.0 USD; Stone Gram Weight: 2.2; Stone Carat Weight: 11.0
Stone Type: EMERALD; Stone Color: GREEN; Stone Translucency: 85%; Stone Cost: 28.0 USD; Stone Gram Weight: 2.0; Stone Carat Weight: 10.0
Stone Type: DIAMOND; Stone Color: COLORLESS; Stone Translucency: 95%; Stone Cost: 30.0 USD; Stone Gram Weight: 2.0; Stone Carat Weight: 10.0

