package by.epam.jewelry.parser;

import java.util.ArrayList;
import java.util.List;

import by.epam.jewelry.entity.AbstractGem;

public abstract class AbstractNecklaceBuilder {

	protected List<AbstractGem> necklace;

	public AbstractNecklaceBuilder() {
		necklace = new ArrayList<AbstractGem>();
	}

	public AbstractNecklaceBuilder(List<AbstractGem> necklace) {
		this.necklace = necklace;
	}

	public List<AbstractGem> getNecklace() {
		return necklace;
	}

	abstract public void buildNecklace(String fileName);

}
