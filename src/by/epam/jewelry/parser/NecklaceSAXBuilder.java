package by.epam.jewelry.parser;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import by.epam.jewelry.entity.AbstractGem;
import by.epam.jewelry.runner.Runner;

public class NecklaceSAXBuilder extends AbstractNecklaceBuilder {
	
	private final static Logger LOG = Logger.getLogger(Runner.class);
	private List<AbstractGem> necklace;
	private NecklaceHandler handler;
	private XMLReader reader;

	public NecklaceSAXBuilder() {
		handler = new NecklaceHandler();
		try {
			reader = XMLReaderFactory.createXMLReader();
			reader.setContentHandler(handler);
		} catch (SAXException e) {
			LOG.error("SAX parser error: " + e);;
		}
	}
	
	public NecklaceSAXBuilder(List<AbstractGem> necklace) {
		super(necklace);
	}

	public List<AbstractGem> getNecklace() {
		return necklace;
	}

	public void buildNecklace(String fileName) {
		try {
			reader.parse(fileName);
		} catch (SAXException e) {
			LOG.error("SAX parser error: " + e);;
		} catch (IOException e) {
			LOG.error("File error" + e);;
		}
		necklace = handler.getNecklace();
	}

}
