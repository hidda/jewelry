package by.epam.jewelry.action;

public class WeightConverter {

	private static final int CONVERT_CONSTANT = 5;

	public static double gramToCarat(double gramWeight) {
		double caratWeight = gramWeight * CONVERT_CONSTANT;
		return caratWeight;
	}

	public static double caratToGram(double caratWeight) {
		double gramWeight = caratWeight / CONVERT_CONSTANT;
		return gramWeight;
	}

}
