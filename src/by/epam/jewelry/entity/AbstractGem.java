package by.epam.jewelry.entity;

public abstract class AbstractGem {

	private String id;
	private int translucency;
	private double cost;
	private double gramWeight;
	private double caratWeight;
	private PhysicalProperties properties = new PhysicalProperties();

	public AbstractGem(String id, int translucency, double cost,
			double gramWeight, double caratWeight, PhysicalProperties properties) {
		this.id = id;
		this.translucency = translucency;
		this.cost = cost;
		this.gramWeight = gramWeight;
		this.caratWeight = caratWeight;
		this.properties = properties;
	}

	public AbstractGem() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getCaratWeight() {
		return caratWeight;
	}

	public void setCaratWeight(double caratWeight) {
		this.caratWeight = caratWeight;
	}

	public int getTranslucency() {
		return translucency;
	}

	public void setTranslucency(int translucency) {
		this.translucency = translucency;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public double getGramWeight() {
		return gramWeight;
	}

	public void setGramWeight(double weight) {
		this.gramWeight = weight;
	}

	public PhysicalProperties getProperties() {
		return properties;
	}

	public void setProperties(PhysicalProperties properties) {
		this.properties = properties;
	}

	public static class PhysicalProperties {

		private double hardness;
		private double specificGravity;
		private Cleavage cleavage;

		public PhysicalProperties(double hardness, double specificGravity,
				Cleavage cleavage) {
			this.hardness = hardness;
			this.specificGravity = specificGravity;
			this.cleavage = cleavage;
		}

		public PhysicalProperties() {
		}

		public double getHardness() {
			return hardness;
		}

		public void setHardness(double hardness) {
			this.hardness = hardness;
		}

		public double getSpecificGravity() {
			return specificGravity;
		}

		public void setSpecificGravity(double specificGravity) {
			this.specificGravity = specificGravity;
		}

		public Cleavage getCleavage() {
			return cleavage;
		}

		public void setCleavage(String cleavage) {
			this.cleavage = Cleavage.valueOf(cleavage.toUpperCase());
		}
	}
}
