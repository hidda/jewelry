package by.epam.jewelry.parser;

public enum NecklaceEnum {

	NECKLACE("necklace"), PRECIOUS_STONE("precious-stone"), SEMI_PRECIOUS_STONE(
			"semi-precious-stone"), ID("id"), TYPE("type"), TRANSLUCENCY(
			"translucency"), COST("cost"), GRAM_WEIGHT("gram-weight"), CARAT_WEIGHT(
			"carat-weight"), HARDNESS("hardness"), SPECIFIC_GRAVITY(
			"specific-gravity"), CLEAVAGE("cleavage"), COLOR("color"), ORIGIN(
			"origin"), PHYSICAL_PROPERTIES("physical-properties");

	private String value;

	private NecklaceEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
