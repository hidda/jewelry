package by.epam.jewelry.runner;

import java.util.List;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import by.epam.jewelry.action.Actions;
import by.epam.jewelry.entity.AbstractGem;
import by.epam.jewelry.entity.Necklace;
import by.epam.jewelry.parser.AbstractNecklaceBuilder;
import by.epam.jewelry.parser.NecklaceBuilderFactory;
import by.epam.jewelry.report.Reporter;

public class Runner {

	private final static String CONFIG_NAME = "config/log4j.xml";
	private final static String XML_ADDRESS = "data/stones.xml";
	private final static Logger LOG = Logger.getLogger(Runner.class);

	static {
		new DOMConfigurator().doConfigure(CONFIG_NAME,
				LogManager.getLoggerRepository());
	}

	public static void main(String[] args) {

		Reporter reporter = new Reporter();
		FileWriter fw = null;
		try {
			fw = reporter.createFileWriter();
		} catch (IOException e) {
			LOG.error("Unable to create File Writer");
		}
		
		NecklaceBuilderFactory factory = new NecklaceBuilderFactory();
		AbstractNecklaceBuilder domBuilder = factory.createNecklaceBuilder("dom");
		domBuilder.buildNecklace(XML_ADDRESS);
		Necklace necklaceDom = new Necklace(domBuilder.getNecklace());
		
		AbstractNecklaceBuilder saxBuilder = factory.createNecklaceBuilder("sax");
		saxBuilder.buildNecklace(XML_ADDRESS);
		Necklace necklaceSax = new Necklace(saxBuilder.getNecklace());
		
		AbstractNecklaceBuilder staxBuilder = factory.createNecklaceBuilder("stax");
		staxBuilder.buildNecklace(XML_ADDRESS);
		Necklace necklaceStax = new Necklace(staxBuilder.getNecklace());
		
		reporter.writeNecklace(necklaceDom.getNecklace(), fw, "Necklace built with DOM Parser: ");
		reporter.writeNecklace(necklaceSax.getNecklace(), fw, "Necklace built with SAX Parser: ");
		reporter.writeNecklace(necklaceStax.getNecklace(), fw, "Necklace built with StAX Parser: ");


		double totalCost = Actions.findTotalCost(necklaceDom.getNecklace());
		reporter.writeTotalCost(totalCost, fw);

		double totalWeight = Actions.findTotalWeight(necklaceDom.getNecklace());
		reporter.writeTotalWeight(totalWeight, fw);

		int translucencyMin = 20;
		int translucencyMax = 80;
		List<AbstractGem> trGems = Actions.translucencyCheck(
				necklaceDom.getNecklace(), translucencyMin, translucencyMax);
		reporter.writeTranslucentGems(trGems, translucencyMin, translucencyMax,
				fw);

		List<AbstractGem> copiedNecklace = Actions.sortNecklace(necklaceDom
				.copyNecklace());
		reporter.writeSortedNecklace(copiedNecklace, fw);

		reporter.closeWriter(fw);

	}
}
