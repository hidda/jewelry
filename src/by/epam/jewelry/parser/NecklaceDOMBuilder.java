package by.epam.jewelry.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import by.epam.jewelry.entity.AbstractGem;
import by.epam.jewelry.entity.PreciousStone;
import by.epam.jewelry.entity.SemiPreciousStone;
import by.epam.jewelry.runner.Runner;

public class NecklaceDOMBuilder extends AbstractNecklaceBuilder {
	private final static Logger LOG = Logger.getLogger(Runner.class);
	private List<AbstractGem> necklace;
	private DocumentBuilder docBuilder;

	public NecklaceDOMBuilder() {
		this.necklace = new ArrayList<AbstractGem>();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			docBuilder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			LOG.error("Parser Configuration Error: " + e);
		}
	}
	
	public NecklaceDOMBuilder(List<AbstractGem> necklace) {
		super(necklace);
	}

	public List<AbstractGem> getNecklace() {
		return necklace;
	}

	public void buildNecklace(String fileName) {
		Document doc = null;
		try {
			doc = docBuilder.parse(fileName);
			Element root = doc.getDocumentElement();
			NodeList preciousStoneList = root.getElementsByTagName("precious-stone");
			for (int i = 0; i < preciousStoneList.getLength(); i++) {
				Element gemElement = (Element) preciousStoneList.item(i);
				PreciousStone preciousGem = buildPreciousStone(gemElement);
				necklace.add(preciousGem);
			}
			NodeList semiPreciousList = root.getElementsByTagName("semi-precious-stone");
			for (int i = 0; i < semiPreciousList.getLength(); i++) {
				Element gemElement = (Element) semiPreciousList.item(i);
				SemiPreciousStone semiPreciousGem = buildSemiPreciousStone(gemElement);
				necklace.add(semiPreciousGem);
			}
		} catch (IOException e) {
			LOG.error("File error: " + e);
		} catch (SAXException e) {
			LOG.error("Parsing failure: " + e);
		}
	}

	private PreciousStone buildPreciousStone(Element gemElement) {
		PreciousStone gem = new PreciousStone();
		gem.setId(gemElement.getAttribute("id"));
		gem.setType(gemElement.getAttribute("type"));
		gem.setColor();
		Double cost = Double.parseDouble(getElementTextContent(gemElement, "cost"));
		gem.setCost(cost);
		Double caratWeight = Double.parseDouble(getElementTextContent(gemElement, "carat-weight"));
		gem.setCaratWeight(caratWeight);
		Double gramWeight = Double.parseDouble(getElementTextContent(gemElement, "gram-weight"));
		gem.setGramWeight(gramWeight);
		Integer translucency = Integer.parseInt(getElementTextContent(gemElement, "translucency"));
		gem.setTranslucency(translucency);
		
		AbstractGem.PhysicalProperties properties = gem.getProperties();
		Element propertiesElement = (Element) gemElement.getElementsByTagName(
				"physical-properties").item(0);
		properties.setCleavage(getElementTextContent(propertiesElement, "cleavage"));
		Double hardness = Double.parseDouble(getElementTextContent(propertiesElement, "hardness"));
		properties.setHardness(hardness);
		Double specificGravity = Double.parseDouble(getElementTextContent(propertiesElement, "specific-gravity"));
		properties.setSpecificGravity(specificGravity);
		return gem;
	}
	
	private SemiPreciousStone buildSemiPreciousStone(Element gemElement) {
		SemiPreciousStone gem = new SemiPreciousStone();
		gem.setId(gemElement.getAttribute("id"));
		Integer translucency = Integer.parseInt(getElementTextContent(gemElement,"translucency"));
		gem.setTranslucency(translucency);
		Double caratWeight = Double.parseDouble(getElementTextContent(gemElement,"carat-weight"));
		gem.setCaratWeight(caratWeight);
		Double gramWeight = Double.parseDouble(getElementTextContent(gemElement,"gram-weight"));
		gem.setGramWeight(gramWeight);
		gem.setType(gemElement.getAttribute("type"));
		gem.setColor(getElementTextContent(gemElement, "color"));
		gem.setOrigin();
		Double cost = Double.parseDouble(getElementTextContent(gemElement,"cost"));
		gem.setCost(cost);
		
		AbstractGem.PhysicalProperties properties = gem.getProperties();
		Element propertiesElement = (Element) gemElement.getElementsByTagName(
				"physical-properties").item(0);
		properties.setCleavage(getElementTextContent(propertiesElement, "cleavage"));
		Double hardness = Double.parseDouble(getElementTextContent(propertiesElement, "hardness"));
		properties.setHardness(hardness);
		Double specificGravity = Double.parseDouble(getElementTextContent(propertiesElement, "specific-gravity"));
		properties.setSpecificGravity(specificGravity);
		return gem;
	}

	private static String getElementTextContent(Element element,
			String elementName) {
		NodeList nList = element.getElementsByTagName(elementName);
		Node node = nList.item(0);
		String text = node.getTextContent();
		return text;
	}
}
