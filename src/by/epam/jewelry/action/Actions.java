package by.epam.jewelry.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import by.epam.jewelry.entity.AbstractGem;

public class Actions {

	public static List<AbstractGem> translucencyCheck(
			List<AbstractGem> necklace, int min, int max) {
		List<AbstractGem> translucentGems = new ArrayList<AbstractGem>();
		for (AbstractGem gem : necklace) {
			if (min <= gem.getTranslucency() && gem.getTranslucency() <= max) {
				translucentGems.add(gem);
			}
		}
		return translucentGems;
	}

	public static double findTotalWeight(List<AbstractGem> necklace) {
		double totalWeight = 0;
		for (AbstractGem gem : necklace) {
			totalWeight += gem.getCaratWeight();
		}
		return totalWeight;
	}

	public static double findTotalCost(List<AbstractGem> necklace) {
		double totalCost = 0;
		for (AbstractGem gem : necklace) {
			totalCost += gem.getCost();
		}
		return totalCost;
	}

	public static List<AbstractGem> sortNecklace(List<AbstractGem> necklace) {
		Comparator<AbstractGem> comparator = new Comparator<AbstractGem>() {
			@Override
			public int compare(AbstractGem gemOne, AbstractGem gemTwo) {
				return Double.compare(gemOne.getCost(), gemTwo.getCost());
			}
		};
		Collections.sort(necklace, comparator);
		return necklace;
	}
}
