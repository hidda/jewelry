package by.epam.jewelry.report;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;

import by.epam.jewelry.entity.AbstractGem;
import by.epam.jewelry.runner.Runner;

public class Reporter {

	private File report = new File("report\\report.txt");
	private final static Logger LOG = Logger.getLogger(Runner.class);

	public FileWriter createFileWriter() throws IOException {
		FileWriter fw = new FileWriter(report);
		try {
			fw.write("Necklace report\n");
		} catch (IOException e) {
			LOG.warn("Unable to write report");
		}
		return fw;
	}

	public void writeNecklace(List<AbstractGem> necklace, FileWriter fw, String str) {
		try {
			fw.write(str);
			fw.write(System.lineSeparator());
			fw.write(System.lineSeparator());
			for (AbstractGem gem : necklace) {
				fw.write(gem.toString());
				fw.write(System.lineSeparator());
			}
			fw.write(System.lineSeparator());
		} catch (IOException e) {
			LOG.warn("Unable to write report");
		}
	}

	public void writeSortedNecklace(List<AbstractGem> copiedNecklace,
			FileWriter fw) {
		try {
			fw.write("After the sorting the necklace consists of: \n\n");
			for (AbstractGem gem : copiedNecklace) {
				fw.write(gem.toString());
				fw.write(System.lineSeparator());
			}
			fw.write(System.lineSeparator());
		} catch (IOException e) {
			LOG.warn("Unable to write report");
		}
	}

	public void writeTotalCost(double totalCost, FileWriter fw) {
		try {
			fw.write("Total Cost of the Necklace: ");
			String strTotalCost = Double.toString(totalCost);
			fw.write(strTotalCost + "\n\n");
		} catch (IOException e) {
			LOG.warn("Unable to write report");
		}
	}

	public void writeTotalWeight(double totalWeight, FileWriter fw) {
		try {
			fw.write("Total Weight of the Necklace: ");
			String strTotalWeight = Double.toString(totalWeight);
			fw.write(strTotalWeight + "\n\n");
		} catch (IOException e) {
			LOG.warn("Unable to write report");
		}
	}

	public void writeTranslucentGems(List<AbstractGem> translucentGems,
			int min, int max, FileWriter fw) {
		try {
			String stringMin = Integer.toString(min);
			String stringMax = Integer.toString(max);
			fw.write("Gems, which translucency is between " + stringMin
					+ " and " + stringMax + ":\n");
			fw.write(System.lineSeparator());
			for (AbstractGem gem : translucentGems) {
				fw.write(gem.toString());
				fw.write(System.lineSeparator());
			}
			fw.write(System.lineSeparator());
		} catch (IOException e) {
			LOG.warn("Unable to write report");
		}
	}

	public void closeWriter(FileWriter fw) {
		if (fw != null) {
			try {
				fw.close();
			} catch (IOException e) {
				LOG.warn("Closing report exception");
			}
		}
	}
}
